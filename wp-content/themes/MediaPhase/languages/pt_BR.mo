��    i      d  �   �       	  
   	  
   	     	  
   	  	   )	     3	     9	     B	     K	  
   X	     c	  
   o	     z	     �	     �	     �	  8   �	     �	     �	     
     

     
     
     &
  	   -
     7
     C
     S
     _
     d
  r   s
     �
  
   �
     �
            W        o          �     �     �  
   �     �     �     �     �     �     �  	         
          %     (     6     E  !   Q  	   s     }     �     �     �     �  	   �     �     �     �  	   �  	   �  	   �     �       F        d     t     �     �     �     �     �     �     �     �  \        _     g     s     x     }     �     �     �     �  )   �     �     �     �     �  	   �                           �  8       
        )     0     ?     M     S     Z     c  	   p     z  
   �     �     �  "   �     �  ,   �               %     ,     2     ;     H     P     Y     e     |     �     �  y   �     .     =     P     X     `  \   o     �     �     �     �          
          2     R     q     ~     �     �     �     �     �     �     �       &   &     M     Z  
   a  	   l     v     z     �     �     �     �  	   �  	   �  	   �     �     �  I   
     T     j  	   }     �     �     �     �     �     �       }        �     �     �     �     �     �     �     �     �  $   �          &     2     8     >     P     X     \     h     k     *             #   O       8   f           c   .   3   T   S       &   U              B               [          :          !          e   \   R      ?                 V   =       ,              P   A   D      I       Q      -              5   ]       "   Z   W   a   9      7   K       $             b   M      <   i   C                  _   Y   6       ^          
   h   @              )   H   	      L           '       +   d   0   1   2   g                         X      4   E   F   /   (   ;   `   G           %      N   >                         J           % Comments %1$s: %2$s %s ago 0 Comments 1 Comment About Activate Archives Archives: %s Author: %s Browse Site Categories Category: %s Comment navigation Comments are closed. Contact Continue reading %s <span class="meta-nav">&rarr;</span> Day: %s Dribbble url Edit Email Facebook Facebook url Footer Galleries Google+ url Header contacts Header logo Home Homepage Setup Icon (see <a href="http://fortawesome.github.io/Font-Awesome/icons/" target="_blank">this link</a> for reference): Icon Color: Icon Size: Image: Images Install Now It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help. Leave a comment Limit: Linkedin url Live Preview Logo Logo image Main Features Main Features Left Main Features Right Member 1 Member 2 Member 3 Month: %s Newer Comments Newer posts No Nothing Found Older Comments Older posts One thought on &ldquo;%2$s&rdquo; Order By: Order: Page %s Pages: Phone Post navigation Posted By Posted in %1$s Posts navigation Primary Menu Product 1 Product 2 Product 3 Products Proudly powered by %s Ready to publish your first post? <a href="%1$s">Get started here</a>. Recent Comments Recent Posts Search Search Results for: %s Select Image Show header contacts Show header logo or text Show shot date? Show shot title? Social Sorry, but nothing matched your search terms. Please try again with some different keywords. Tag: %s Tagged %1$s Team Text Text Widget Text: Theme: %1$s by %2$s. Title Title: Try looking in the monthly archives. %1$s Twitter Twitter url Type: URL: Username: Year: %s Yes just now on themefurnace Advertisement Project-Id-Version: MediaPhase
POT-Creation-Date: 2018-05-16 09:44-0300
PO-Revision-Date: 2018-05-16 09:45-0300
Last-Translator: daniel tygel <dtygel@eita.org.br>
Language-Team: 
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.7.1
X-Poedit-Basepath: .
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: _;_e;__;_nx;_x
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: ..
 % comentários %1$s: %2$s há %s 0 comentários 1 comentário Sobre Ativar Arquivos Arquivos: %s Autor: %s Navegar site Categorias Categoria: %s Navegar em comentários Comentários estão desabilitados. Contato Mais %s <span class="meta-nav">&rarr;</span> Dia: %s Dribbble url Editar Email Facebook Facebook url Rodapé Galerias Google+ url Contatos no cabeçalho Logo do cabeçalho Início Configurar página principal Ícone (veja <a href="http://fortawesome.github.io/Font-Awesome/icons/" target="_blank">this link</a> para referências): Cor do ícone: Tamanho do ícone: Imagem: Imagens Instalar agora Parece que não conseguimos encontrar o que vocês está procurando. Talvez uma busca ajude. Deixe um comentário Limite: Linkedin url Prever ao vivo Logo Imagem (logomarca) Principais serviços Principais serviços (esquerda) Principais serviços (direita) Integrante 1 Integrante 2 Integrante 3 Mês: %s Comentários mais recentes Postagens mais recentes Não Nenhum resultado encontrado Comentários mais antigos Postagens mais antigas Um pensamento sobre &ldquo;%2$s&rdquo; Ordenar por: Ordem: Página %s Páginas: Tel Navegue na postagem Enviado por Enviado em %1$s Navegação no blog Menu principal Produto 1 Produto 2 Produto 3 Produtos Site realizado por %s Pronto para publicar seu primeiro artigo? <a href="%1$s">Comece aqui</a>. Comentários recentes Postagens recentes Pesquisar Buscar resultados para: %s Selecione a imagem Exibir contatos no cabeçalho Exibir logo ou texto Mostrar a data? Mostrar o título? Menu social Desculpe, mas não achamos nenhum resultado para os termos de pesquisa. Por favor, tente novamente com outras palavras-chave. Tag: %s Tageado como %1$s Equipe Texto Widget de texto Texto: Tema: %1$s por %2$s. Título Título: Tente ver nos arquivos mensais. %1$s Twitter Twitter url Tipo: Link: Nome de usuário: Ano: %s Sim agora mesmo em themefurnace Advertisement 