<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package mediaphase
 */
?>

<div id="backtotop">
	<div class="wrap">
		<a href="#"><i class="fa fa-chevron-up"></i></a>
	</div><!-- End #wrap -->
</div>

<div id="footer">
	<div class="wrap">
		<?php
		if ( is_active_sidebar( 'mediaphase-footer' ) ) {
			dynamic_sidebar( 'mediaphase-footer' );
		}
		?>
	</div><!-- End #wrap -->

	<br><hr>

	<div class="wrap">
		<div class="footerwidget-double">
			<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14805.077997580544!2d-46.3879717!3d-21.9241886!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x7d6aea1215610b44!2sAssessoria+Cont%C3%A1bil+Celina+Generoso+Ltda.!5e0!3m2!1spt-BR!2sbr!4v1526476130584" frameborder="0" style="width:100%;height:400px;border:0" allowfullscreen></iframe>
		</div>
	</div>
</div>

</div><!-- End .container -->

<?php wp_footer(); ?>

</body>
</html>
